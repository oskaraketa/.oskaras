﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hotel.ViewModels;
using Hotel.Services;
using Hotel.Dal.Enumerations;
using Hotel.Main.Utils;
using System.ComponentModel.DataAnnotations;

namespace Hotel.Main
{
    public partial class RoomList : Form
    {
        private List<RoomViewModel> _rooms;
        private RoomAdminService _roomAdminService;
        public RoomList()
        {
            _roomAdminService = new RoomAdminService();
            InitializeComponent();
            ConfigureDataGridView();
            FillDataGridView();
        }

        private void MockData()
        {
            _rooms.Add(
                new RoomViewModel {
                    RoomId = 1,
                    RoomNumber = 111,
                    RoomType = RoomTypeEnum.Single
                }
            );
        }

        private void FillDataGridView()
        {
            _rooms = _roomAdminService.GetRooms();

            if (_rooms == null)
            {
                var index = dataGridRooms.Rows.Add();
                dataGridRooms.Rows[index].Cells["RoomNumber"].Value = "No Data";

            } else
            {
                foreach (var room in _rooms)
                {
                    var index = dataGridRooms.Rows.Add();
                    dataGridRooms.Rows[index].Cells["RoomNumber"].Value = room.RoomNumber;
                    dataGridRooms.Rows[index].Cells["RoomType"].Value = room.RoomType.GetAttribute<DisplayAttribute>().Name;
                    dataGridRooms.Rows[index].Cells["roomObject"].Value = room;
                }
            }
        }

        private void ConfigureDataGridView()
        {
            dataGridRooms.Columns[0].Resizable = DataGridViewTriState.False;
            dataGridRooms.Columns["RoomId"].CellTemplate = new DataGridViewTextBoxCell();

            dataGridRooms.Columns.Add("RoomNumber", "Room Number");
            dataGridRooms.Columns["RoomNumber"].Width = 100;
            dataGridRooms.Columns["RoomNumber"].Resizable = DataGridViewTriState.False;
            dataGridRooms.Columns["RoomNumber"].CellTemplate = new DataGridViewTextBoxCell();

            DataGridViewColumn RoomType = new DataGridViewColumn
            {
                Width = 130,
                HeaderText = "Room Type",
                Name = "RoomType",
                Resizable = DataGridViewTriState.False,
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridRooms.Columns.Add(RoomType);

            DataGridViewButtonColumn editRoom = new DataGridViewButtonColumn
            {
                Width = 60,
                HeaderText = "",
                Name = "EditRoom",
                CellTemplate = new DataGridViewButtonCell(),
                Text = "Edit",
                UseColumnTextForButtonValue = true,
                Resizable = DataGridViewTriState.False
            };
            dataGridRooms.Columns.Add(editRoom);

            DataGridViewColumn roomObject = new DataGridViewColumn
            {
                Visible = false,
                Name = "roomObject",
                CellTemplate = new DataGridViewTextBoxCell()
            };
            dataGridRooms.Columns.Add(roomObject);
        }

        private void dataGridRooms_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                var room = (RoomViewModel)((DataGridView)sender).Rows[e.RowIndex].Cells["roomObject"].Value;
                var roomSaveForm = new RoomSave(room);
                roomSaveForm.ShowDialog();
            }
            
        }
    }
}
