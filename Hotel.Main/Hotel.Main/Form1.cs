﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace Hotel.Main
{
    public partial class Form1 : Form
    {
        //private string _connectionString = "server=127.0.0.1;uid=root;pwd=root;database=hotelnkkm";
        private string _connectionString = ConfigurationManager.ConnectionStrings["hotelConnectionString"].ConnectionString;
        private MySqlConnection conn;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var reservationForm = new Reservation();
            reservationForm.Show();
        }
    }
}