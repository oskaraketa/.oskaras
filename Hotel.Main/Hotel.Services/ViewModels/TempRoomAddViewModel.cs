﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hotel.ViewModels
{
    public class TempRoomAddViewModel
    {
        public int? Id { get; set; }

        public int? ReservationId { get; set; }

        public int RoomTypeId { get; set; }

        public int AccommodationTypeId { get; set; }

        public int Quantity { get; set; }
    }
}
