﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hotel.Dal.Enumerations;

namespace Hotel.Dal.Entities
{
    public class RoomType
    {
        public int? RoomTypeId { get; set; }

        public RoomTypeEnum _RoomType { get; set; } //Kad butu fancy
        public float PricePerNight { get; set; }

        private RoomType()
        {

        }

        public RoomType(RoomTypeEnum roomType, float pricePerNight, int? roomTypeId = null)
        {
            _RoomType = roomType;
            PricePerNight = pricePerNight;
            RoomTypeId = roomTypeId;
        }
    }
}
